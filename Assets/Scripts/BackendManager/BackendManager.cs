﻿using System.Collections;
using UnityEngine;
using System.Net;
using Newtonsoft.Json;
using System;
using UnityEngine.Networking;

public class BackendManager : Singleton<BackendManager>
{
    #region Private Variables
    private const string
        ERROR = "Result Not Available!",
        NO_INTERNET = "Invalid";

    private bool _isConnected = false;
    private UIManager _uiManager;
    #endregion

    #region Public Variables
    public GameObject _internetError;
    #endregion

    #region Unity Callbacks
    private void Start()
    {
        _uiManager = UIManager.Instance;

        StartCoroutine(CheckInternetConnection((isConnected) =>
        {
            _isConnected = isConnected;

            if (_isConnected == false)
            {
                _internetError.SetActive(true);
                return;
            }
        }));
    }
    #endregion

    #region Generate Currency
    // will create the list of currencies available
    public string[] GenerateCurrency() 
    {
        Rates rates = new Rates();

        var values = rates.GetType();

        string[] currencies = new string[values.GetProperties().Length];

        for (int i = 0; i < values.GetProperties().Length; i++)
        {
            var value = values.GetProperties().GetValue(i).ToString();
            var currency = value.Substring(value.Length - 3);

            currencies[i] = currency;
        }

        return currencies;
    }
    #endregion

    #region Functions To Handle Exchange Rate & Calculations

    public void GetRate(ExchangeState state, ExchangeData exchange)
    {
        if (_isConnected == true)
        {
            string url = exchange.GetUrl(state);

            var jsonData = "";

            StartCoroutine(GetData(url, (data) =>
            {
                jsonData = JsonToResult(data, exchange.endCurrency);

                switch (state)
                {
                    case ExchangeState.GO_LIVE:
                        _uiManager._liveRateText.text = jsonData;
                        break;
                    case ExchangeState.GET_EXCHANGE:
                        if (jsonData == ERROR)
                            _uiManager._liveRateWithAmountText.text = jsonData;
                        else
                        {
                            var res = jsonData.Remove(jsonData.Length - 4);
                            float rate = float.Parse(res);
                            var newResult = rate * exchange.amount;
                            string fin = newResult + " " + exchange.endCurrency;

                            _uiManager._liveRateWithAmountText.text = fin;
                        }

                        break;
                    default:
                        _uiManager._liveRateWithDate.text = jsonData;
                        break;
                }

            }));
        }
        else
        {
            _internetError.SetActive(true);

            switch (state)
            {
                case ExchangeState.GO_LIVE:
                    _uiManager._liveRateText.text = NO_INTERNET;
                    break;
                case ExchangeState.GET_EXCHANGE:
                    _uiManager._liveRateWithAmountText.text = NO_INTERNET;
                    break;
                default:
                    _uiManager._liveRateWithDate.text = NO_INTERNET;
                    break;
            }
        }
    }

    /// <summary>
    /// Checks the internet connection.
    /// </summary>
    /// <returns>The internet connection.</returns>
    /// <param name="action">Action.</param>
    private IEnumerator CheckInternetConnection(Action<bool> action)
    {
        using (UnityWebRequest request = UnityWebRequest.Get("http://google.com"))
        {
            yield return request.SendWebRequest();

            if (request.error != null)
            {
                action(false);
            }
            else
            {
                action(true);
            }
        }
    }


    /// <summary>
    /// Get the Data From Server
    /// </summary>
    /// <param name="url"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    private IEnumerator GetData(string url, Action<string> data)
    {
        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
            }
            else
            {
                data(request.downloadHandler.text);
            }
        }
    }

    /// <summary>
    /// Gets Json Data and Return Result Accordingly
    /// </summary>
    /// <param name="data"></param>
    /// <param name="endCurrency"></param>
    /// <returns></returns>
    private string JsonToResult(string data, string endCurrency)
    {
        var jsonData = "";
        var exchange = JsonConvert.DeserializeObject<Exchange>(data);
        var values = exchange.rates.GetType();
        var currency = GenerateCurrency();

        int i = 0;
        for (; i < currency.Length; i++)
        {
            if (currency[i] == endCurrency)
            {
                jsonData = values.GetProperty(currency[i]).GetValue(exchange.rates, null) + " " + currency[i];
                break;
            }
        }

        if (i >= currency.Length || string.IsNullOrEmpty(jsonData))
            jsonData = ERROR;

        return jsonData;
    }

    #endregion
}