﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UIManager : Singleton<UIManager>
{
    #region Private Variables
    private const string CURRENCY = "currency";
    private string _currencyA, _currencyB, _amount, _day, _month, _year;
    private BackendManager _backendManager;
    private Dropdown[] _dropdown = new Dropdown[2];

    #endregion

    #region Public Variables
    public Text _liveRateText, _liveRateWithAmountText, _liveRateWithDate;
    #endregion

    #region Unity Callbacks
    void Start()
    {
        _backendManager = BackendManager.Instance;

        GetCurrencySlots();
    }
    #endregion

    #region Manages Currency Display Area
    private void GetCurrencySlots() 
    {
        GameObject[] slots = GameObject.FindGameObjectsWithTag(CURRENCY);

        var currencies = _backendManager.GenerateCurrency();

        foreach(GameObject slot in slots) 
        {
            var currencyList = slot.GetComponent<Dropdown>();
            currencyList.options.Clear();
            currencyList.options.AddRange(from string currency in currencies
                                          let data = new Dropdown.OptionData { text = currency }
                                          select data);
        } 
    }
    #endregion

    #region Event Calls

    public void CurrencyA(Text currency) 
    {
        _currencyA = currency.text;
    }

    public void CurrencyB(Text currency)
    {
        _currencyB = currency.text;
    }

    public void Amount(Text amount) 
    {
        _amount = amount.text;
    }

    public void OptionValueA(Dropdown dropdown) 
    {
        _dropdown[0] = dropdown;
    }

    public void OptionValueB(Dropdown dropdown)
    {
        _dropdown[1] = dropdown;
    }

    public void Day(Text day)
    {
        _day = day.text;
    }

    public void Month(Text month)
    {
        _month = month.text;
    }

    public void Year(Text year)
    {
        _year = year.text;
    }

    public void PerformCurrencyInverse() 
    {
        int value = _dropdown[0].value;

        _dropdown[0].value = _dropdown[1].value;

        _dropdown[1].value = value;
    }

    public void LiveRate()
    {
        ExchangeData data = new ExchangeData
        {
            sourceCurrency = _currencyA,
            endCurrency = _currencyB
        };

        _backendManager.GetRate(ExchangeState.GO_LIVE, data);
    }

    public void LiveRateWithAmount() 
    {
        ExchangeData data = new ExchangeData
        {
            sourceCurrency = _currencyA,
            endCurrency = _currencyB,
            amount = float.Parse(_amount)
        };

        _backendManager.GetRate(ExchangeState.GET_EXCHANGE, data);
    }

    public void LiveRateWithDate()
    {
        ExchangeData data = new ExchangeData
        {
            sourceCurrency = _currencyA,
            endCurrency = _currencyB,
            day = _day,
            month = _month,
            year = _year,
        };

        _backendManager.GetRate(ExchangeState.GET_HISTORY, data);
    }

    #endregion
}
